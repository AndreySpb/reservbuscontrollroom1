package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.10.v20211216-rNA", date="2023-03-14T18:37:31")
@StaticMetamodel(Drivers.class)
public class Drivers_ { 

    public static volatile SingularAttribute<Drivers, String> birthday;
    public static volatile SingularAttribute<Drivers, String> surname;
    public static volatile SingularAttribute<Drivers, String> name;
    public static volatile SingularAttribute<Drivers, Integer> id;
    public static volatile SingularAttribute<Drivers, String> family;
    public static volatile SingularAttribute<Drivers, Integer> status;

}