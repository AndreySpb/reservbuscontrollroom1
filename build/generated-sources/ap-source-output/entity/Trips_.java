package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.10.v20211216-rNA", date="2023-03-14T18:37:31")
@StaticMetamodel(Trips.class)
public class Trips_ { 

    public static volatile SingularAttribute<Trips, Integer> busId;
    public static volatile SingularAttribute<Trips, Integer> routeId;
    public static volatile SingularAttribute<Trips, Integer> driverId;
    public static volatile SingularAttribute<Trips, String> dtStr;
    public static volatile SingularAttribute<Trips, String> timeStr;
    public static volatile SingularAttribute<Trips, Integer> del;
    public static volatile SingularAttribute<Trips, Integer> id;

}