/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Buses;
import entity.Cities;
import entity.Drivers;
import entity.Routes;
import entity.TicketSale;
import entity.Trips;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import session.BusManager;
import session.BusesFacade;
import session.CitiesFacade;
import session.DriverManager;
import session.DriversFacade;
import session.RouterManager;
import session.RoutesFacade;
import session.TicketManager;
import session.TicketSaleFacade;
import session.TripsFacade;
import session.TripsManager;

/**
 *
 * @author andrey
 */
@WebServlet(name = "web_controller", 
        loadOnStartup = 1,
        urlPatterns = {
            "/buses", 
            "/drivers", 
            "/trips", 
            "/routers", 
            "/tickets", 
            "/add_bus", 
            "/add_driver", 
            "/add_raice", 
            "/add_route", 
            "/add_ticket",
            "/driver",
            "/bus",
            "/trip",
            "/route",
            "/registr_new_driver",
            "/register_new_bus",
            "/register_new_route",
            "/register_new_trip",
            "/register_new_ticket"
        }
)
public class web_controller extends HttpServlet {
    
    @EJB
    DriversFacade driversFacade;
    
    @EJB
    BusesFacade busesFacade;
    
    @EJB
    CitiesFacade citiesFacade;
    
    @EJB
    RoutesFacade routesFacade;
    
    @EJB
    TripsFacade tripsFacade;
    
    @EJB
    TicketSaleFacade ticketSaleFacade;
    
    @EJB
    DriverManager driverManager;
    
    @EJB
    BusManager busManager;
    
    @EJB
    RouterManager routerManager;
    
    @EJB
    TripsManager tripManager;
    
    @EJB
    TicketManager ticketManager;
    
    @Override
    public void init() throws ServletException {
        this.getDriversForTable(); //заполняем таблицу с водителями
        
        this.getBusesForTable(); // заполняем таблицу с автогбусами
        
        this.getRoutesForTable(); //заполняем таблицу с маршрутами
        
        this.getTripsForTable();
    }
    
    /**
     * Получаем данные для таблицы водителей
     * добавляем расшифровку статуса
     */
    private void getDriversForTable(){
        String[] ststus = {"уволен","работает","простаивает","отпуск","больничный","уволен"};
        List drivers = new ArrayList();

        List<Drivers> results = driversFacade.findAll();
        for(int i = 0; i<results.size(); i++){
            Map<String, String> map = new HashMap<String, String>();
            
            map.put("id", Integer.toString(results.get(i).getId()));
            
            map.put("family", results.get(i).getFamily());
            map.put("name", results.get(i).getName());
            map.put("surname", results.get(i).getSurname());
            map.put("birthday", results.get(i).getBirthday());
            map.put("status", ststus[results.get(i).getStatus()]);
            drivers.add(map);
        }
        getServletContext().setAttribute("drivers", drivers); 
    }
    
    private void getBusesForTable(){
        String[] ststus = {"","работает","простаивает","сломан","списан","уволен"};
        List buses = new ArrayList();

        List<Buses> results = busesFacade.findAll();
        for(int i = 0; i<results.size(); i++){
            Map<String, String> map = new HashMap<String, String>();
            
            map.put("id", Integer.toString(results.get(i).getId()));
            
            map.put("name", results.get(i).getName());
            map.put("qntPlace", Integer.toString(results.get(i).getQntPlace()));
            map.put("nomer", results.get(i).getNomer());
            map.put("status", ststus[results.get(i).getStatus()]);
            buses.add(map);
        }
        getServletContext().setAttribute("buses", buses);
    }
    
    /**
     * Получаем строки для маршрутов из БД
     * переделываем, чтобы вывести с именами городов
     */
    private void getRoutesForTable(){
        List routers = new ArrayList();

        List<Routes> results = routesFacade.findAll();
        for(int i = 0; i<results.size(); i++){
            Map<String, String> map = new HashMap<String, String>();
            map.put("id", Integer.toString(results.get(i).getId()));
            map.put("cityIdOut", getNameCity(results.get(i).getCityIdOut()));
            map.put("cityIdIn", getNameCity(results.get(i).getCityIdOut()));
            map.put("length", Float.toString(results.get(i).getLength()));
            map.put("travelTime", results.get(i).getTravelTime());
            map.put("price", Float.toString(results.get(i).getPrice()));
            routers.add(map);
        }
        
        getServletContext().setAttribute("routers", routers);
    }
    
    /**
     * Составное название для селектора маршрута 
     */
    private void getRouteNames(){
        List routers = new ArrayList();

        List<Routes> results = routesFacade.findAll();
        for(int i = 0; i<results.size(); i++){
            String routeName = this.getNameCity(results.get(i).getCityIdOut()) + " - " +this.getNameCity(results.get(i).getCityIdIn());
            Map<String, String> map = new HashMap<String, String>();
            map.put("id", Integer.toString(results.get(i).getId()));
            map.put("name", routeName);
            
            routers.add(map);
        }
        
        getServletContext().setAttribute("routes", routers);
    }
    
    /**
     * Строки для таблицы 
     */
    private void getTripsForTable(){
        List trips = new ArrayList();
        List<Trips> results = tripsFacade.findAll();
        
        for(int i = 0; i < results.size(); i++){
            Map<String, String> busData = this.getDataBus(results.get(i).getBusId());
            Map<String, String> routeData = this.getDataRoute(results.get(i).getRouteId());
            
            Integer tripId = results.get(i).getId(); 
            Integer qntSale = this.getSalesTicket(tripId);
            String qntPlace = busData.get("qntPlace");
            Integer qnrFree = Integer.valueOf(qntPlace) - qntSale;
            
            Map<String, String> map = new HashMap<String, String>();
            map.put("id", Integer.toString(tripId));
            map.put("tripName", routeData.get("nameRoute"));
            map.put("dtStr", results.get(i).getDtStr());
            map.put("timeStr", results.get(i).getTimeStr());
            map.put("timeEnd", this.getTimeEnd(results.get(i).getTimeStr(), routeData.get("travelTime")));
            map.put("busId", Integer.toString(results.get(i).getBusId()));
            map.put("bus", busData.get("nameBus"));
            map.put("driverId", Integer.toString(results.get(i).getDriverId()));
            map.put("driver", this.getDriverName(results.get(i).getDriverId()));
            map.put("qntPlace", qntPlace);
            map.put("qntSales", Integer.toString(qntSale));
            map.put("qntFree", Integer.toString(qnrFree));
            trips.add(map);
        }
        getServletContext().setAttribute("trips", trips);
    }
    
    /**
     * Получаем время прибытия
     * @param timeStr
     * @param travelTime
     * @return 
     */
    private String getTimeEnd(String timeStr, String travelTime){
        
        String[] times = timeStr.split(":");
        Integer hour = Integer.parseInt(times[0])+Integer.parseInt(travelTime);
        Integer h;
        String newTime = times[0];
        if(hour < 10){
            newTime = "0" + hour;
        }else if(hour > 23){
            h = hour - 24;
            newTime = (h < 10)? "0"+h: Integer.toString(h);
        }
        String minuts = "00";
        if(times.length > 1){
            minuts = times[1];
        }
        return newTime + ":" + minuts;
    }
    
    /**
     * Количество проданых билетов у рейса
     * @param tripId
     * @return 
     */
    private Integer getSalesTicket(Integer tripId){
        List ticketList = ticketSaleFacade.findByTripId(tripId);
        return ticketList.size();
    }
    
    /**
     * Получаем данные по маршруту
     * @param id
     * @return 
     */
    private Map<String, String> getDataRoute(Integer id){
        Routes route = routesFacade.find(id);
        Map<String, String> map = new HashMap<String, String>();
        map.put("nameRoute", this.getNameCity(route.getCityIdOut()) + " - " +this.getNameCity(route.getCityIdIn()));
        map.put("travelTime", route.getTravelTime());
        return map;
    }
    
    /**
     * Получаем данные по автобусу
     * @param id
     * @return 
     */
    private Map<String, String> getDataBus(Integer id){
        Buses bus = busesFacade.find(id);
        Map<String, String> map = new HashMap<String, String>();
        map.put("nameBus", bus.getNomer() + ":" + bus.getName());
        map.put("qntPlace", Integer.toString(bus.getQntPlace()));
        return map;
    }
    
    /**
     * Пулучаем имя водителя
     */
    private String getDriverName(Integer id){
        Drivers driver = driversFacade.find(id);
        return driver.getFamily() + " " + driver.getName();
    }
    /**
     * Получаем название автобуса 
     * @param id
     * @return 
     */
    private String getBusName(Integer id){
        Buses bus = busesFacade.find(id);
        
        return bus.getNomer() + ":" + bus.getName();
    }
    
    /**
     * Получаем название маршрута по его ID
     * @param id
     * @return 
     */
    private String getRouteName(Integer id){
        Routes route = routesFacade.find(id);
        return this.getNameCity(route.getCityIdOut()) + "-" + this.getNameCity(route.getCityIdIn());
    }
    
    /**
     * Получаем название города
     * @param id
     * @return 
     */
    private String getNameCity(Integer id){
        Cities city = citiesFacade.find(id);
        return city.getName();
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String userPath=request.getServletPath();
        String fileView = "/WEB-INF/views"+userPath+".jsp";
        
        Enumeration<String> parameters = request.getParameterNames();
        
        String id = null, status = null, name = null, price = null;
        Integer codeOperation;
        
        switch (userPath) {
            case "/routers":
                
            break;
            case "/register_new_ticket":
                String tripId = null;
                while (parameters.hasMoreElements()) {
                    String parameter = parameters.nextElement();
                    if(parameter.equals("tripId")){
                        tripId = request.getParameter(parameter);
                    }else
                    if(parameter.equals("price")) {
                        price = request.getParameter(parameter);
                    }
                }

                codeOperation = ticketManager.add(Integer.valueOf(tripId), Float.valueOf(price));
                //codeOperation = ticketManager.add(2, 300.0F);
                
                if (codeOperation!=0){
                    request.setAttribute("notif", "Рэйс не добавлен");
                }
                else{
                    request.setAttribute("notif", "Рэйс успешно создан!");
                }
                
                fileView = "/WEB-INF/views/add_ticket.jsp";
                break;
            case "/register_new_trip":
                String routeId = null, dtStr = null, timeStr = null, busId = null, driverId = null;
                while (parameters.hasMoreElements()) {
                    String parameter = parameters.nextElement();
                    if(parameter.equals("routeId")){
                        routeId = request.getParameter(parameter);
                    }else
                    if(parameter.equals("dtStr")) {
                        dtStr = request.getParameter(parameter);
                    }else
                    if(parameter.equals("timeStr")) {
                        timeStr = request.getParameter(parameter);
                    }else
                    if(parameter.equals("busId")) {
                        busId = request.getParameter(parameter);
                    }else
                    if(parameter.equals("driverId")) {
                        driverId = request.getParameter(parameter);
                    }    
                    
                }
                codeOperation = tripManager.addTrips(
                        Integer.parseInt(routeId), 
                        dtStr, 
                        timeStr, 
                        Integer.parseInt(busId), 
                        Integer.parseInt(driverId)
                );
                
                if (codeOperation!=0){
                    request.setAttribute("notif", "Билет не добавлен");
                }
                else{
                    request.setAttribute("notif", "Билет успешно зарегистрирован!");
                }
                fileView = "/WEB-INF/views/add_raice.jsp";
                break;
            case "/register_new_route":
                String outPoint = null, inPoint = null, travel_time = null, length = null;
                while (parameters.hasMoreElements()) {
                    String parameter = parameters.nextElement();
                    if(parameter.equals("outPoint")){
                        outPoint = request.getParameter(parameter);
                    }else
                    if (parameter.equals("inPoint")){
                        inPoint = request.getParameter(parameter);
                    }else
                    if (parameter.equals("travel_time")){
                        travel_time = request.getParameter(parameter);
                    }else
                    if (parameter.equals("length")){
                        length = request.getParameter(parameter);
                    }else
                    if (parameter.equals("price")){
                        price = request.getParameter(parameter);
                    }
                }
                codeOperation = routerManager.addRoute(
                        Integer.parseInt(inPoint),
                        Integer.parseInt(outPoint), 
                        travel_time, 
                        Float.parseFloat(length), 
                        Float.parseFloat(price)
                );
                if (codeOperation!=0){
                    request.setAttribute("notif", "Маршрут не добавлен");
                }
                else{
                    request.setAttribute("notif", "маршрут  "+ outPoint + "-" + inPoint + " успешно создан!");
                }
                fileView = "/WEB-INF/views/add_router.jsp";
                break;
            case "/register_new_bus":
                String nomer = null, qntPlace = null;
                while (parameters.hasMoreElements()) {
                    String parameter = parameters.nextElement();
                    if(parameter.equals("nomer")){
                        nomer = request.getParameter(parameter);
                    }else
                    if (parameter.equals("name")){
                        name = request.getParameter(parameter);
                    }else
                    if (parameter.equals("qntPlace")){
                        qntPlace = request.getParameter(parameter);
                    }else
                    if (parameter.equals("status")){
                        status = request.getParameter(parameter);
                    }
                }
                codeOperation = busManager.addBus(nomer, name, Integer.parseInt(qntPlace), Integer.parseInt(status));
                if (codeOperation!=0){
                    request.setAttribute("notif", "Автобус не добавлен");
                }
                else{
                    request.setAttribute("notif", "Автобус № "+ nomer +" успешно создан!");
                }
                fileView = "/WEB-INF/views/add_bus.jsp";
                break;
                
            case "/registr_new_driver":
                String family = null, surname = null, birthday = null;
                while (parameters.hasMoreElements()) {
                    String parameter = parameters.nextElement();
                    
                    if (parameter.equals("family")){
                        family=request.getParameter(parameter);
                    }else
                    if (parameter.equals("name")){
                        name = request.getParameter(parameter);
                    }else
                    if (parameter.equals("surname")){
                        surname = request.getParameter(parameter);
                    }else
                    if (parameter.equals("birthday")){
                        birthday = request.getParameter(parameter);
                    }else
                    if (parameter.equals("status")){
                        status = request.getParameter(parameter);
                    }
                }
                codeOperation = driverManager.addDriver(family, name, surname, birthday, Integer.parseInt(status));
                if (codeOperation!=0){
                    request.setAttribute("notif", "Водитель не добавлен");
                }
                else{
                    request.setAttribute("notif", "Водитель "+family + " " + name + " " + surname +" успешно создан!");
                }
                fileView = "/WEB-INF/views/add_driver.jsp";
                break;
                
            case "/bus":
                while (parameters.hasMoreElements()) {
                    String param = parameters.nextElement();
                    id="id".equals(param)?request.getParameter(param):id;
                }
                try{
                    Buses bus = busesFacade.find(Integer.valueOf(id));
                    request.setAttribute("bus", bus);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                }
                fileView = "/WEB-INF/views/add_bus.jsp";
                break;
            case "/driver":
                
                while (parameters.hasMoreElements()) {
                    String param = parameters.nextElement();
                    id="id".equals(param)?request.getParameter(param):id;
                }
                try{
                    Drivers driver = driversFacade.find(Integer.valueOf(id));
                    request.setAttribute("driver", driver);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                }
                fileView = "/WEB-INF/views/add_driver.jsp";
                break;
                //TODO: вывод данных по айди рейса
            case "/trip":
                this.getRouteNames(); // id_route and route_name
                getServletContext().setAttribute("buses", busesFacade.findAll());
                getServletContext().setAttribute("drivers", driversFacade.findAll());
                
                while (parameters.hasMoreElements()) {
                    String param = parameters.nextElement();
                    id="id".equals(param)?request.getParameter(param):id;
                }
                try{
                    Trips trip = tripsFacade.find(Integer.valueOf(id));
                    request.setAttribute("trip", trip);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                }
                fileView = "/WEB-INF/views/add_raice.jsp";
                break;
                //TODO: вывод данных по айди маршрута
            case "/route":
                getServletContext().setAttribute("cities", citiesFacade.findAll());
                
                while (parameters.hasMoreElements()) {
                    String param = parameters.nextElement();
                    id = "id".equals(param)?request.getParameter(param):id;
                }
                
                try{
                    Routes route = routesFacade.find(Integer.valueOf(id));
                    request.setAttribute("route", route);
                }catch(NumberFormatException e){
                    e.printStackTrace();
                }
                fileView = "/WEB-INF/views/add_router.jsp";
                break;
                //TODO: при создании билета передается id рейса на который продается билет
            case "/add_ticket":
                /*Продажа билета на рейс*/
                while (parameters.hasMoreElements()) {
                    String param = parameters.nextElement();
                    id = "id".equals(param)?request.getParameter(param):id;
                }
                if(id != null){
                    request.setAttribute("trip", this.formSaleTicket(Integer.valueOf(id)));
                }else{
                    request.setAttribute("notif", "Номер рейса не передан");
                }
                fileView = "/WEB-INF/views/add_ticket.jsp";
                break;
        }
        request.getRequestDispatcher(fileView).forward(request, response);
    }
    
    /**
     * продажа билета на рейс
     * @param tripId
     * @return 
     */
    public Map<String, String> formSaleTicket(Integer tripId){
        this.getRouteNames(); //for selector: id_route and route_name
        //data trip
        Trips trip = tripsFacade.find(tripId);
        Map<String, String> map = new HashMap<String, String>();
        map.put("tripId", Integer.toString(trip.getId()));
        map.put("dtStr", trip.getDtStr());
        Routes route = routesFacade.find(trip.getRouteId());
        map.put("routeId", Integer.toString(trip.getRouteId()));
        map.put("price", Float.toString(route.getPrice()));
        
        return map;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
