/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package session;

import entity.TicketSale;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andrey
 */
@Stateless
public class TicketSaleFacade extends AbstractFacade<TicketSale> {

    @PersistenceContext(unitName = "WebApplication6PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TicketSaleFacade() {
        super(TicketSale.class);
    }
    
    /**
     * Получаем список рэйсов по указанному маршруту 
     * @param tripId
     * @return 
     */
    public List findByTripId(Integer tripId){
        List resultList = em.createNamedQuery("TicketSale.findByTripId").setParameter("tripId", tripId).getResultList();
        return resultList;
    }
    
}
