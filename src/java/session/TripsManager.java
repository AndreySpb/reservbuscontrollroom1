/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package session;

import entity.Trips;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andrey
 */
@TransactionManagement(TransactionManagementType.CONTAINER)
@Stateless
public class TripsManager {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName = "WebApplication6PU")
    private EntityManager entityManager;
    
    @Resource
    private SessionContext context;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer addTrips(Integer routeId, String dtStr, String timeStr, Integer busId, Integer driverId) {
            try{
                Trips trip = newTrip(routeId, dtStr, timeStr, busId, driverId);
                return 0;
            }catch (Exception e){
                context.setRollbackOnly();
                e.printStackTrace();
                return 1;
            }
    }
    
    private Trips newTrip(Integer routeId, String dtStr, String timeStr, Integer busId, Integer driverId) {
        Trips trip = new Trips();
        trip.setRouteId(routeId);
        trip.setBusId(busId);
        trip.setDriverId(driverId);
        trip.setDtStr(dtStr);
        trip.setTimeStr(timeStr);
        trip.setDel(0);

        entityManager.persist(trip);
        return trip;
    }
}
