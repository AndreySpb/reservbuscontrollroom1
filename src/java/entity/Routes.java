/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andrey
 */
@Entity
@Table(name = "routes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Routes.findAll", query = "SELECT r FROM Routes r"),
    @NamedQuery(name = "Routes.findById", query = "SELECT r FROM Routes r WHERE r.id = :id"),
    @NamedQuery(name = "Routes.findByCityIdIn", query = "SELECT r FROM Routes r WHERE r.cityIdIn = :cityIdIn"),
    @NamedQuery(name = "Routes.findByCityIdOut", query = "SELECT r FROM Routes r WHERE r.cityIdOut = :cityIdOut"),
    @NamedQuery(name = "Routes.findByTravelTime", query = "SELECT r FROM Routes r WHERE r.travelTime = :travelTime"),
    @NamedQuery(name = "Routes.findByLength", query = "SELECT r FROM Routes r WHERE r.length = :length"),
    @NamedQuery(name = "Routes.findByPrice", query = "SELECT r FROM Routes r WHERE r.price = :price"),
    @NamedQuery(name = "Routes.findByDel", query = "SELECT r FROM Routes r WHERE r.del = :del")})
public class Routes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "city_id_in")
    private Integer cityIdIn;
    @Column(name = "city_id_out")
    private Integer cityIdOut;
    @Size(max = 45)
    @Column(name = "travel_time")
    private String travelTime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "length")
    private Float length;
    @Column(name = "price")
    private Float price;
    @Column(name = "del")
    private Integer del;

    public Routes() {
    }

    public Routes(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCityIdIn() {
        return cityIdIn;
    }

    public void setCityIdIn(Integer cityIdIn) {
        this.cityIdIn = cityIdIn;
    }

    public Integer getCityIdOut() {
        return cityIdOut;
    }

    public void setCityIdOut(Integer cityIdOut) {
        this.cityIdOut = cityIdOut;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getDel() {
        return del;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Routes)) {
            return false;
        }
        Routes other = (Routes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Routes[ id=" + id + " ]";
    }
    
}
