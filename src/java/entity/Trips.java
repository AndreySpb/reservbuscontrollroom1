/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andrey
 */
@Entity
@Table(name = "trips")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Trips.findAll", query = "SELECT t FROM Trips t"),
    @NamedQuery(name = "Trips.findById", query = "SELECT t FROM Trips t WHERE t.id = :id"),
    @NamedQuery(name = "Trips.findByRouteId", query = "SELECT t FROM Trips t WHERE t.routeId = :routeId"),
    @NamedQuery(name = "Trips.findByDriverId", query = "SELECT t FROM Trips t WHERE t.driverId = :driverId"),
    @NamedQuery(name = "Trips.findByBusId", query = "SELECT t FROM Trips t WHERE t.busId = :busId"),
    @NamedQuery(name = "Trips.findByDtStr", query = "SELECT t FROM Trips t WHERE t.dtStr = :dtStr"),
    @NamedQuery(name = "Trips.findByTimeStr", query = "SELECT t FROM Trips t WHERE t.timeStr = :timeStr"),
    @NamedQuery(name = "Trips.findByDel", query = "SELECT t FROM Trips t WHERE t.del = :del")})
public class Trips implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "route_id")
    private Integer routeId;
    @Column(name = "driver_id")
    private Integer driverId;
    @Column(name = "bus_id")
    private Integer busId;
    @Size(max = 45)
    @Column(name = "dt_str")
    private String dtStr;
    @Size(max = 45)
    @Column(name = "time_str")
    private String timeStr;
    @Column(name = "del")
    private Integer del;

    public Trips() {
    }

    public Trips(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getBusId() {
        return busId;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public String getDtStr() {
        return dtStr;
    }

    public void setDtStr(String dtStr) {
        this.dtStr = dtStr;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Integer getDel() {
        return del;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trips)) {
            return false;
        }
        Trips other = (Trips) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Trips[ id=" + id + " ]";
    }
    
}
