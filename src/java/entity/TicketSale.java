/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andrey
 */
@Entity
@Table(name = "ticket_sale")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TicketSale.findAll", query = "SELECT t FROM TicketSale t"),
    @NamedQuery(name = "TicketSale.findById", query = "SELECT t FROM TicketSale t WHERE t.id = :id"),
    @NamedQuery(name = "TicketSale.findByTripId", query = "SELECT t FROM TicketSale t WHERE t.tripId = :tripId"),
    @NamedQuery(name = "TicketSale.findByDtSale", query = "SELECT t FROM TicketSale t WHERE t.dtSale = :dtSale"),
    @NamedQuery(name = "TicketSale.findByPrice", query = "SELECT t FROM TicketSale t WHERE t.price = :price")})
public class TicketSale implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "trip_id")
    private Integer tripId;
    @Size(max = 255)
    @Column(name = "dt_sale")
    private String dtSale;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Float price;

    public TicketSale() {
    }

    public TicketSale(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public String getDtSale() {
        return dtSale;
    }

    public void setDtSale(String dtSale) {
        this.dtSale = dtSale;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TicketSale)) {
            return false;
        }
        TicketSale other = (TicketSale) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TicketSale[ id=" + id + " ]";
    }
    
}
